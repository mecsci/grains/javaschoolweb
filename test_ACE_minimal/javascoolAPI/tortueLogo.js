Method Summary


//La tortue recule de n pas.
function backward(double n){} 

//Efface toutes traces du carré de salade de taille (512, 512).
function clear_all(){}

//La tortue avance de n pas.
function forward(double n) 

function hide_turtle(){} 

//Retour au milieu du carré de salade, au point (256, 256).
function home(){}


//La tortue tourne de n degrés d'angle vers la gauche.
function leftward(double n){}

//La tortue laisse sa trace (par défaut).
function pen_down(){}

//
function pen_up(){} 

//La tortue ne laisse pas de trace.
function rightward(double n){}

//La tortue tourne de n degrés d'angle vers la droite.
function set_background(int n){}

//Change la couleur du fond, n est un entier positif entre 0 et 9.
function set_color(int n){}

//Change la couleur du crayon, n est un entier positif entre 0 et 9.
function set_heading(double a){}

//Fixe le cap de la tortue de maniere absolue, selon l'angle de a degrés.
function set_position(double x, double y){}

//Fixe la position absolue de la tortue dans le carré de salade.
function show_turtle(){}