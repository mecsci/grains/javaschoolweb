/**
 * 
 */
function OneDriveConnector(app_client_id, redirect_url){

	var APP_CLIENT_ID = app_client_id;
	var REDIRECT_URL = redirect_url;
	//var SCOPE = "wl.signin wl.skydrive wl.basic wl.birthday wl.emails";	//"wl.skydrive";
	var SCOPE = "wl.signin wl.skydrive wl.basic wl.birthday wl.emails";
	   
	var DOCUMENTS_FOLDER_ID;
	var FILES =  new Object();
	var NEW_DIR = new Object();
	var ROOT_DIR;
	var session;
	
	if ( typeof OneDriveConnector.initialized == "undefined" ) {
		
		
		/**
		 * 
		 */
		OneDriveConnector.prototype.init = function(){
			log("client initialization ...");
			WL.init({ client_id: APP_CLIENT_ID, redirect_uri: REDIRECT_URL });
			//WL.Event.subscribe("auth.login", onLogin);
			//WL.Event.subscribe("auth.sessionChange", onSessionChange);
			//session = WL.getSession();
			console.log("initialization done");
		};
		
		/**
		 * 
		 */
		OneDriveConnector.prototype.checkSession = function(){
			if (session) {
				log("You are already signed in!");
			} else {
				log("Please login.");
			}
		};
		
		/**
		 * 
		 */
		OneDriveConnector.prototype.signInUser = function(){
			log("signInUser function called !");
			if (!session) {
				WL.login({
					scope: SCOPE
				});
			}
		};
		
		/**
		 * 
		 */
		OneDriveConnector.prototype.logout = function(){
			log("logout function called !");
			WL.logout();
		};
		
		
		
		/**
		 * 
		 */
		OneDriveConnector.prototype.showUserData = function(){
			log("showUserData function called !");
			WL.api({ path: "/me", method: "GET" }).then(
					function(response) {
						fillRegistrationForm(response);
					},
					function(response) {
						log("API call failed: " + JSON.stringify(response.error).replace(/,/g, "\n"));
					}
			);
		};
		
		
		
		/**
		 * 
		 */
		OneDriveConnector.prototype.listFolders = function(subFolderByID, callback) {
			log("listing folders : "+subFolderByID+'....');
			if(subFolderByID.length != 0) {
				var FilesPath = "/"+subFolderByID+"/files";
			}else {
				var FilesPath = "/me/skydrive/files";
			}

			WL.api({ path: FilesPath, method: "GET" }).then(
					onGetFoldersComplete,
					function(response) {
						var jsonFeed = JSON.stringify(response.error).replace(/,/g, ",\n")
						log("Cannot get files and folders: " + jsonFeed);


					}
			);
			
			log(".... listing folders : "+subFolderByID+'is finished')
			
		};
		
		
		OneDriveConnector.prototype.listFolders2 = function(folderId,callback) {
			console.log('OneDrive API listing fodler : '+folderId);
			var folderPath = undefined;
			if(folderId == null || folderId == undefined){
				folderPath = "me/skydrive/files";
			}else{
				folderPath = "/"+folderId+"/files";
				
			}
			WL.api({path: folderPath,	method: "GET"}).then(
					function(response) {
						//console.log("success : "+response);
						//console.log(response);
						callback(response); 
					}, 
					function (responseFailed) {
						console.log("failed : "+responseFailed); 
						console.log("failed : "+ responseFailed.error.message);
					}
			);
		};
		
		
		
		var onGetFoldersComplete = function(response) {
			var items = response.data;
			var foundFolder = 0;
			console.log(items);
			console.log("items length : "+items.length);
			
			for (var i = 0; i < items.length; i++) {
				
				console.log("item name :"+items[i].name+" , type :"+items[i].type+" id : "+items[i].id);
				
				if (items[i].type === "folder" || items[i].type === "album") {
					
//					var jsonFeed = JSON.stringify(items[i]).replace(/,/g, ",\n");
//					var parsedJson = JSON && JSON.parse(jsonFeed) || $.parseJSON(jsonFeed);
//					//log("Found a folder with the following information: " + jsonFeed);
//					log("Found a folder with the following information: " + parsedJson);
//					console.log(parsedJson);
//					log("Folder name: " + parsedJson.name);
//					log("Folder type: " + parsedJson.type);
//					//log("Folder id: " + parsedJson.id);
//					//log("Folder parent_id: " + parsedJson.parent_id);
//					//log("Folder upload_location: " + parsedJson.upload_location);
//					//log("Folder link: " + parsedJson.link);
//					//log("Folder created_time: " + parsedJson.created_time);
//					//log("Folder updated_time: " + parsedJson.updated_time);
//					//log("Folder shared_with: " + (parsedJson.shared_with).access);
//					//log("Folder count: " + parsedJson.count+ "<p></p>");
					foundFolder += 1;
//					if(parsedJson.count != 0) {
//						listFolders(parsedJson.id); //if we find a folder, list its contents.
					console.log("try to call listFolders ...");
					this.listFolders(items[i].upload_location);
					console.log("call to listFolders must be finished");
//					}
	//
//					if(parsedJson.name == "Documents") {
//						DOCUMENTS_FOLDER_ID = parsedJson.id;
//						//we know Documents parent is the root directory.
//						ROOT_DIR = parsedJson.parent_id;
//					}
//					NEW_DIR[parsedJson.id] = parsedJson.parent_id;
				}
				if (items[i].type === "file"){
//					var jsonFileFeed = JSON.stringify(items[i]).replace(/,/g, ",\n");
//					var parsedJson = JSON && JSON.parse(jsonFileFeed) || $.parseJSON(jsonFileFeed);
	//
//					//log("Found a folder with the following information: " + jsonFileFeed);
//					log("<p></p>"+"File name: " + parsedJson.name);
//					log("File id: " + parsedJson.id);
//					log("File parent_id: " + parsedJson.parent_id);
//					log("File upload_location: " + parsedJson.upload_location);
//					log("File link: " + parsedJson.link);
//					log("File created_time: " + parsedJson.created_time);
//					log("File updated_time: " + parsedJson.updated_time);
	//
//					log("File shared_with: " + (parsedJson.shared_with).access + "<p></p>");
//					NEW_DIR[parsedJson.id] = parsedJson.parent_id;
				}

//				FILES[parsedJson.id] = parsedJson;
			}

			if (foundFolder == 0) {
				log("Unable to find any folders");
			}
		}
		
		
		
	}
	
	
	$( "#listFolders" ).click(function() {
		listFolders("");
	});
	$( "#getQuota" ).click(function() {
		getQuota();
	});
	$( "#getFiles" ).click(function() {
		getFiles();
	});
	
	
	function onLogin() {
		var session = WL.getSession();
		if (session) {
			log("You are signed in!");
		}
	}
	
	
	
	function onSessionChange() {
		var session = WL.getSession();
		if (session) {
			log("Your session has changed.");
		}
	}
	function showUserData() {
		
	}



	

	
	
	
	function getFiles() {
		logRight("NAME of file/folder : PARENT NAME of file/folder<br />")
		for ( property in NEW_DIR ) {
			logRight( property + " : " + NEW_DIR[property] + "<br />"); // Outputs: foo, fiz or fiz, foo
		}

	}
	function getQuota() {
		WL.api({ path: "/me/skydrive/quota", method: "GET" }).then(
				function(response) {
					log(JSON.stringify(response).replace(/,/g, ",\n"));
				},
				function(response) {
					log("Could not access quota, status = " +
							JSON.stringify(response.error).replace(/,/g, ",\n"));
				}
		);
	}

	function log(message) {
		console.log("OneDrive API -> "+message);
//		var child = document.createTextNode(message);
//		var parent = document.getElementById('JsOutputDiv') || document.body;
//		parent.appendChild(child);
//		parent.appendChild(document.createElement("br"));
	}
	function logRight(message) {
		$("#JsOutputDivRight").append(message+"<br />");
	}

}