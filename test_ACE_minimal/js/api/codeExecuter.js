/**
 * this library contains all the fonctions used to execute a javscool code
 */


/**
* javascool API URL
*/
var javscoolAPI = "http://site-local.dev/git/JavascoolWeb/test_ACE_minimal/js/javascoolAPI.js";

/**
 * jquery API URL
 */
//"http://localhost/jquery-1.11.2.min.js";
var jqueryAPI = "http://site-local.dev/git/JavascoolWeb/test_ACE_minimal/js/jquery-1.11.2.min.js";


var canvasAPI = "http://site-local.dev/git/JavascoolWeb/test_ACE_minimal/js//canvasAPI.js";


//TODO to convert to an object



/**
 * Parse the code in parameter and a line tag for each end of line character.
 * the line tag will be a comment formed like : {LX}  (where X is the line number)
 * @param code the code to parse
 * @returns {String} the code in parameter with the lines tags
 */
function addLineTags(code){	

	var index = 0;
	var result = code.replace(/\n/g, function myFunction(x){
		++ index;
		return buildLineTag(index)+"\n";
	});

	return result+buildLineTag(++index);
};


function buildLineTag(index){
	return"/*@JVSL:"+index+"@*/";
}


/**
 * fonction used to execute the javascript code
 * 
 * @param code the code to execute
 */
function execute(code) {
	
	code = addLineTags(code);		/* the first who must be done*/
	code = addExceptionsBlock(code);	/* the second who must ne done*/

	code = "<script>" + code + "<\/script>";
	/* 		var data_url = "data:text/html;charset=utf-8;base64,"+ $.base64.encode(code);
	 */

	code = addJavascriptLibrary(code, javscoolAPI);	//TODO to optimize with array
	code = addJavascriptLibrary(code, jqueryAPI);	//add the jquery API
	code = addJavascriptLibrary(code, canvasAPI);	//add the canvas API
	var data_url = "data:text/html;charset=utf-8," + code;

	document.getElementById("output").src = data_url;

}

/**
 * Append a javascript library to the code
 * 
 * @param code the code taht we append the javascript
 * @param library the javascript library source
 * @returns {String} the code with
 */
function addJavascriptLibrary(code, library){
	return "<script src=\""+library+"\"></script>"+code;
}



/**
 * Surround the code in parameter by a try catch block
 * 
 * @param code the code to edit
 * @returns {String} the code surrounded by the tryc catch block
 */
function addExceptionsBlock(code){
	var i = 0;
	if(getBrowser() == navigator_enum.CHROME){
		i = javscoolAPI.length + jqueryAPI.length + (24*2); //24 for blocks <script></script>
	}
	if(getBrowser() == navigator_enum.FIREFOX){
		i  = 9; //because we add : function(){
	}
	return "try{"+code+"}catch(e){printError("+i+", e, function(){"+code+"});}";
}