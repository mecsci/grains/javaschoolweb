function BrowserStorage(){


	var STORAGE_MODE = {
			LOCAL : "local",
			SESSION: "session"
	};

	if ( typeof BrowserStorage.initialized == "undefined" ) {



		/**
		 * Permits to get the data save in the local storage
		 */
		BrowserStorage.prototype.getLocalData = function(key){

			return JSON.parse(localStorage.getItem(key));
		};


		/**
		 * Permits to get the data save in the session storage
		 */
		BrowserStorage.prototype.getSessionData = function(key){

			return JSON.parse(sessionStorage.getItem(key));
		};


		var getData = function(filename, content){

			var data = null;
			if(filename === null || content === null){
				data = null;
			}else{
				data = createData(filename, content);
			}

			return data;
		};


		/**
		 * private function used to save datas
		 */
		var saveData = function(object,key, override, mode){

			var result = null;

			var toSave = JSON.stringify(object);
			
			if(mode == STORAGE_MODE.LOCAL){

				var file = localStorage.getItem(key);
				if((file == null) || (file != null && override)){

					localStorage[key] = toSave;
				}

			}
			if(mode == STORAGE_MODE.SESSION){

				var file = sessionStorage.getItem(key);
				if((file == null) || (file != null && override)){
			
					sessionStorage[key] = toSave;
				}
			}

			result = JSON.parse(toSave);

			return result;
		};


		/**
		 * Permits to delete the data saved in the local storage
		 */
		BrowserStorage.prototype.deleteLocalData = function(key){

			localStorage.removeItem(key);
		}	



		/**
		 * Permits to delete the data save in the session storage
		 */
		BrowserStorage.prototype.deleteSessionData = function(key){

			sessionStorage.removeItem(key);
		}


		/**
		 * Permits to save data in the local storage.
		 * @param data the data to save
		 * @param override boolean with true value if we want to override the data already saved
		 * @return the data saved
		 */
		BrowserStorage.prototype.saveLocalData = function(object,key, override){

			return saveData(object, key, override, STORAGE_MODE.LOCAL)
		}


//		/**
//		 * 
//		 */
//		BrowserStorage.prototype.saveLocalSettings = function(data){
//
//			localStorage[storageKey_settings] = JSON.stringify(data);
//		}		
//
//
//		/**
//		 * 
//		 */
//		BrowserStorage.prototype.getLocalSettings = function(){
//
//			return JSON.parse(localStorage.getItem(storageKey_settings));
//		}



		/**
		 * Permits to save data in the session storage.
		 * @param data the data to save
		 * @param override boolean with true value if we want to override the data already saved
		 * @preturn the data saved
		 */
		BrowserStorage.prototype.saveSessionData= function(object,key, override){

			return saveData(object, key, override, STORAGE_MODE.SESSION)
		}



		/**
		 * check if the Storage (local or Session) functionalities supported by the browser
		 */
		BrowserStorage.prototype.checkStorageFunctionalities = function(){

			if(typeof(Storage) === "undefined") {
				throw new Error("local storage functionnalities are not supported by this browser");
			}

		}



	}
};