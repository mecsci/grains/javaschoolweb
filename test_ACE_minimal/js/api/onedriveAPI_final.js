/**
 * 
 */
function OneDriveConnector(app_client_id, redirect_url){

	var APP_CLIENT_ID = app_client_id;
	var REDIRECT_URL = redirect_url;
	var SCOPE = "wl.signin wl.skydrive";
	var DOCUMENTS_FOLDER_ID;
	var FILES =  new Object();
	var NEW_DIR = new Object();
	var ROOT_DIR;
	var session;


	if ( typeof OneDriveConnector.initialized == "undefined" ) {


		var onLogin = function() {
			var session = WL.getSession();
			if (session) {
				log("You are signed in!");
			}
		}

		var onSessionChange = function() {
			var session = WL.getSession();
			if (session) {
				log("Your session has changed.");
			}
		}


		/**
		 * 
		 */
		OneDriveConnector.prototype.init = function(){
			console.log("oneDrive Connector final init function called !!!!!!!!!!");
			WL.init({ client_id: APP_CLIENT_ID, redirect_uri: REDIRECT_URL });
			//WL.Event.subscribe("auth.login", onLogin);
			//WL.Event.subscribe("auth.sessionChange", onSessionChange);
			//this.session = WL.getSession();
		};



		OneDriveConnector.prototype.signInUser = function() {
			console.log("oneDrive Connector final signInUser function called !");
			if (!session) {
				WL.login({
					scope: SCOPE
				}).then(
						function (response) {
							console.log("signInUser done !!!!");                
						}, 
						function (responseFailed) {
							console.log( 
									"signInUser failed !!! " + responseFailed.error.message);
						}
				);
			}
		};

		OneDriveConnector.prototype.signOutUser = function() {
			WL.logout();
		};


		/**
		 * Permits to check if oneDrive session is active (already logged on
		 */
		OneDriveConnector.prototype.checkSessionActive = function(){
			var result;
			if (this.session) {
				console.log("You are already signed in!");
				result = true;
			} else {
				console.log("You are not signed in!");
				resutl = false;
			}
			return result;
		};



		OneDriveConnector.prototype.listFolders = function (subFolderByID, callback) {
			if(subFolderByID != null && subFolderByID.length != 0) {
				var FilesPath = "/"+subFolderByID+"/files";
			}else {
				var FilesPath = "/me/skydrive/files";
			}

			WL.api({ 
				path: FilesPath, method: "GET" 
			}).then(
					function (response) {

						//adding isFile or isFolder properties
						for(var i=0; i< response.data.length;i++){
							if(response.data[i].type === 'file'){
								response.data[i].isFile=true;
							}else{
								response.data[i].isFolder=true;
							}
						}

						callback(response.data);
					}, 
					function (responseFailed) {
						console.log( 
								"Error reading folder properties: " + responseFailed.error.message);
					}
			);
		};


		OneDriveConnector.prototype.readFile = function (path, callback) {

			WL.api({
				path: path+"/content",
				method: "GET"
			}).then(
					function (response) {
						console.log(response);
						callback(response.location);
					},
					function (responseFailed) {
						console.log(responseFailed);
					}); 
		};


		OneDriveConnector.prototype.listFolders2 = function(folderId,callback) {
			console.log('OneDrive API listing fodler : '+folderId);
			var folderPath = undefined;
			if(folderId == null || folderId == undefined){
				folderPath = "me/skydrive/files";
			}else{
				folderPath = "/"+folderId+"/files";

			}
			WL.api({path: folderPath,	method: "GET"}).then(
					function(response) {
						//console.log("success : "+response);
						//console.log(response);
						callback(response); 
					}, 
					function (responseFailed) {
						console.log("failed : "+responseFailed); 
						console.log("failed : "+ responseFailed.error.message);
					}
			);
		};


		}
	}






//	if (session) {
//	log("You are already signed in!");
//	} else {
//	log("Please login.");
//	}
//	$( "#login" ).click(function() {
//	signInUser();
//	});
//	$( "#logout" ).click(function() {
//	WL.logout();
//	log("Please login.");
//	});
//	$( "#showUserData" ).click(function() {
//	showUserData();
//	});
//	$( "#listFolders" ).click(function() {
//	listFolders("");
//	});
//	$( "#getQuota" ).click(function() {
//	getQuota();
//	});
//	$( "#getFiles" ).click(function() {
//	getFiles();
//	});
//	function onLogin() {
//	var session = WL.getSession();
//	if (session) {
//	log("You are signed in!");
//	}
//	}


//	function signInUser() {
//	console.log("signInUser function called !");
//	if (!session) {
//	WL.login({
//	scope: SCOPE
//	});
//	}
//	}


//	function onSessionChange() {
//	var session = WL.getSession();
//	if (session) {
//	log("Your session has changed.");
//	}
//	}

//	function showUserData() {
//	WL.api({ path: "/me", method: "GET" }).then(
//	function(response) {
//	fillRegistrationForm(response);
//	},
//	function(response) {
//	log("API call failed: " + JSON.stringify(response.error).replace(/,/g, "\n"));
//	}
//	);
//	}

//	function fillRegistrationForm(user) {
////	NOTE: Assign these values to your form elements to streamline registration.
//	log("First name: " + user.first_name);
//	log("Last name: " + user.last_name);
//	log("Preferred email: " + user.emails.preferred);
//	log("Gender: " + user.gender);
//	log("Birthday: " + user.birth_month + "/" + user.birth_day + "/" + user.birth_year);
//	}

//	function listFolders(subFolderByID) {
//	if(subFolderByID.length != 0) {
//	var FilesPath = "/"+subFolderByID+"/files";
//	}else {
//	var FilesPath = "/me/skydrive/files";
//	}

//	WL.api({ path: FilesPath, method: "GET" }).then(
//	onGetFoldersComplete,
//	function(response) {
//	var jsonFeed = JSON.stringify(response.error).replace(/,/g, ",\n")
//	log("Cannot get files and folders: " + jsonFeed);


//	}
//	);
//	}

//	function onGetFoldersComplete(response) {
//	var items = response.data;
//	var foundFolder = 0;
//	for (var i = 0; i < items.length; i++) {
//	if (items[i].type === "folder") {
//	var jsonFeed = JSON.stringify(items[i]).replace(/,/g, ",\n");
//	var parsedJson = JSON && JSON.parse(jsonFeed) || $.parseJSON(jsonFeed);
////	log("Found a folder with the following information: " + jsonFeed);
//	log("<p></p>"+"Folder name: " + parsedJson.name);
//	log("Folder id: " + parsedJson.id);
//	log("Folder parent_id: " + parsedJson.parent_id);
//	log("Folder upload_location: " + parsedJson.upload_location);
//	log("Folder link: " + parsedJson.link);
//	log("Folder created_time: " + parsedJson.created_time);
//	log("Folder updated_time: " + parsedJson.updated_time);
//	log("Folder shared_with: " + (parsedJson.shared_with).access);
//	log("Folder count: " + parsedJson.count+ "<p></p>");
//	foundFolder += 1;
//	if(parsedJson.count != 0) {
//	listFolders(parsedJson.id); //if we find a folder, list its contents.
//	}

//	if(parsedJson.name == "Documents") {
//	DOCUMENTS_FOLDER_ID = parsedJson.id;
////	we know Documents parent is the root directory.
//	ROOT_DIR = parsedJson.parent_id;
//	}
//	NEW_DIR[parsedJson.id] = parsedJson.parent_id;
//	}
//	if (items[i].type === "file"){
//	var jsonFileFeed = JSON.stringify(items[i]).replace(/,/g, ",\n");
//	var parsedJson = JSON && JSON.parse(jsonFileFeed) || $.parseJSON(jsonFileFeed);

////	log("Found a folder with the following information: " + jsonFileFeed);
//	log("<p></p>"+"File name: " + parsedJson.name);
//	log("File id: " + parsedJson.id);
//	log("File parent_id: " + parsedJson.parent_id);
//	log("File upload_location: " + parsedJson.upload_location);
//	log("File link: " + parsedJson.link);
//	log("File created_time: " + parsedJson.created_time);
//	log("File updated_time: " + parsedJson.updated_time);

//	log("File shared_with: " + (parsedJson.shared_with).access + "<p></p>");
//	NEW_DIR[parsedJson.id] = parsedJson.parent_id;
//	}

//	FILES[parsedJson.id] = parsedJson;
//	}

//	if (foundFolder == 0) {
//	log("Unable to find any folders");
//	}
//	}
//	function getFiles() {
//	logRight("NAME of file/folder : PARENT NAME of file/folder<br />")
//	for ( property in NEW_DIR ) {
//	logRight( property + " : " + NEW_DIR[property] + "<br />"); // Outputs: foo, fiz or fiz, foo
//	}

//	}
//	function getQuota() {
//	WL.api({ path: "/me/skydrive/quota", method: "GET" }).then(
//	function(response) {
//	log(JSON.stringify(response).replace(/,/g, ",\n"));
//	},
//	function(response) {
//	log("Could not access quota, status = " +
//	JSON.stringify(response.error).replace(/,/g, ",\n"));
//	}
//	);
//	}

//	function log(message) {
//	console.log();
//	$("#JsOutputDiv").append(message+"<br />");
////	var child = document.createTextNode(message);
////	var parent = document.getElementById('JsOutputDiv') || document.body;
////	parent.appendChild(child);
////	parent.appendChild(document.createElement("br"));
//	}

//	function logRight(message) {
//	$("#JsOutputDivRight").append(message+"<br />");
//	}
