
/**
 * Pour ce canvas les coordonnées des points sont comprises entre 
 * -width,width et -height, height
 * le point de coordonné 0,0 est donc situé au milieur du canvas
 */
function MyCanvas(canvasID){

	this.canvas = document.getElementById(canvasID);
	this.context = this.canvas.getContext("2d");
	this.ratio = 2;
	//ctx.fillStyle = "#FF0000";



	if ( typeof MyCanvas.initialized == "undefined" ) {

		
		//----------------------INTERNAL FUNCTIONS------------
		/**
		 * fonction interne qui permert de convertir les coordonnees d'un point
		 * dans le repere du canvas
		 */
		var convertPoint = function(canvas, ratio, x, y, color){
			
			var px = x + (canvas.width/ratio);
			var py =  (canvas.height/ratio) -y;
			return {x:px, y:py};
		};
		
		//----------------------------------------------------
		

		/**
		 * efface le contenu du canvas
		 */
		MyCanvas.prototype.clear = function(){
			this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
		};




		/**
		 * initialize le canvas de largeur width *2 et de hauteur height *2
		 */
		MyCanvas.prototype.reset = function(width, height){
			this.canvas.width = width * this.ratio;
			this.canvas.height = height * this.ratio;
		};


		/**
		 * Pour ajouter une étiquette "s" de couleur c au point x, y:
		 */
		MyCanvas.prototype.addString = function(x, y, text, color){
			//convertion des coordonnees dans le repère du canvas
			var p = convertPoint(this.canvas,this.ratio, x,y);

			this.context.fillStyle = color;
			this.context.fillText(text, p.x, p.y);
		};


		/**
		 * Pour ajouter une ligne de couleur c du point x1, y1 au point x2, y2 :
		 */
		MyCanvas.prototype.addLine = function(x1, y1, x2, y2, color){
			//convertion des coordonnees dans le repère du canvas
			var p1 = convertPoint(this.canvas,this.ratio, x1,y1);
			var p2 = convertPoint(this.canvas,this.ratio, x2,y2);


			this.context.beginPath();
			this.context.strokeStyle = color;
			this.context.moveTo(p1.x, p1.y);
			this.context.lineTo(p2.x, p2.y);
			this.context.stroke();
		};


		/**
		 * Pour ajouter un cercle de couleur c de centre x, y et de rayon r :
		 */
		MyCanvas.prototype.addCircle = function(x, y, radius, color){
			//convertion des coordonnees dans le repère du canvas
			var p = convertPoint(this.canvas,this.ratio, x,y);

			this.context.beginPath();
			this.context.strokeStyle = color;
			this.context.arc(p.x, p.y, radius, 0, 2 * Math.PI, false);
			this.context.stroke();
		};


		/**
		 * 
		 */
//		MyCanvas.prototype.getX = function(){

//		};


		/**
		 * 
		 */
//		MyCanvas.prototype.getY = function(){

//		};


		/**
		 * 
		 */
		var loadImage = function(url, canvas, context){
			var imageObj = new Image();
			imageObj.onload = function() {
				context.drawImage(imageObj, 0, 0);
				
			};
			imageObj.src = url;
		}
		
		
		
		MyCanvas.prototype.load = function(url){
			
			var img = document.getElementById('myPicture');
			img.src=url;
			
			
			loadImage(url, this.canvas, this.context);
			console.log("loadding done !!")
			//get pixels from picture
			//var imgData = imageObj.data;//context.getImageData(0, 0, canvas.width, canvas.height);
			//clearing canvas
			this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
			//console.log(imageObj);
			
			
			// load image from data url
			
			var buffer = [[]];
			buffer[0][10] = 1;

		};


		/**
		 * retourne la largeur du canvas
		 */
		MyCanvas.prototype.getWidth = function(){
			return this.canvas.width / this.ratio;
		};



		/**
		 * retourne la hauteur du canvas
		 */
		MyCanvas.prototype.getHeight = function(){
			return this.canvas.height / this.ratio;
		};


		/**
		 * retourne le pixel de coordonnees (x,y) 
		 */
		MyCanvas.prototype.getPixel = function(x,y){
			//convertion des coordonnees dans le repère du canvas
			var p = convertPoint(this.canvas,this.ratio, x,y);
			return this.context.getImageData(p.x, p.y, 1, 1);
		};

		
		
		/**
		 * modifie le pixel de coordonnees (x,y) avec la couleur c
		 */
		MyCanvas.prototype.setPixel = function(x, y, color){
			//convertion des coordonnees dans le repère du canvas
			var p = convertPoint(this.canvas,this.ratio, x,y);
			
			this.context.beginPath();
			this.context.fillStyle = color;
			this.context.rect(p.x, p.y, 1, 1);
			this.context.fill();
			this.context.stroke();
		};


		/**
		 * modifie le pixel de coordonnees (x,y) avec la couleur c
		 */
		MyCanvas.prototype.setPoint = function(x, y, color){
			this.setPixel(x, y, color);
		};


		/**
		 * retourne la couleur (sous la forme d'un tableau) d'une pixel
		 * de coordonnees donnees (x,y)
		 */
		MyCanvas.prototype.getPixelColor = function(x, y){
			return this.getPixel(x, y).data;
		};



		/**
		 * 
		 */
		MyCanvas.prototype.getUrl = function(){
			return this.canvas.toDataURL();

		};


	};

};

