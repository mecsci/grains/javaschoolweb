
/*------------------- PUBLIC FONCTIONS --------------*/
function DropboxConnector(app_key){


	this.APP_KEY = app_key;
	this.client = null;	//new Dropbox.Client({ key: this.APP_KEY });
	this.files = null;


	if ( typeof DropboxConnector.initialized == "undefined" ) {

		/**
		 * 
		 */
		DropboxConnector.prototype.init = function(){
			this.client = new Dropbox.Client({ key: this.APP_KEY });
			this.client.onError.addListener(function(error) {
				return showError(error);
			});
		};



		/**
		 * 
		 */
		DropboxConnector.prototype.authenticateClient = function(){
			console.log("Dropbox : authenticateClient called");
			this.client.authenticate(function(error) {
				if (error) {
					return showError(error);

				}
			});
		};



		/**
		 * permits to write a file in dropox
		 */
		DropboxConnector.prototype.writeFile = function (filename, content) {
			client.writeFile(filename, content, function (error) {
				if (error) {
					console.log('Error: ' + error);
				} else {
					console.log('File: ['+filename+'] written successfully!');
				}
			});
		};


		/**
		 * Permits to read the file content
		 * @param path	the file path
		 * @param callback	the callback use to get the result
		 */
		DropboxConnector.prototype.readFile = function(path, callback){
			this.client.readFile(path, function(error, content){
				if (error) {
					return showError(error); // Something went wrong.
				}

				callback(content);
			});


		};


		/**
		 * Permmits to get the content from a folder
		 * @param callback the callboack used to get the result
		 * @param path tthe path that you want to read;
		 */
		DropboxConnector.prototype.listContent = function(path, callback){

			console.log("dropboxAPI: -> listContent function called");

			this.client.readdir(path, function(error, listFiles, directoryInfo, folderEntries) {
				if (error) {
					return showError(error); // Something went wrong.
				}

				console.log("dropboxAPI: -> listContent ;");
				console.log(folderEntries);
				callback(folderEntries);
			});

		};
		

		DropboxConnector.prototype.checkCredentialsAvailable = function(){
			this.client.authenticate({interactive: false}, function(error, client){
				if(error){
					return showError(error);
				}else{
					console.log("checkCredentialsAvailable successfull !!!");
				}
			});
		};


		/**
		 * Checks if this client can perform API calls on behalf of a user.
		 * @return true if this client has a user's OAuth 2 access token and can be used to make API calls; 
		 * false otherwise
		 */
		DropboxConnector.prototype.verifyClientIsAuthenticated = function(){
			return this.client.isAuthenticated();
		};

		

		/**
		 * Invalidates and forgets the user's Dropbox OAuth 2 access token.
		 * This should be called when the user explicitly signs off from your application, 
		 * to meet the users' expectation that after they sign out, their access tokens will not be persisted on the machine.
		 */
		DropboxConnector.prototype.disconnectClient = function(){
			this.client.signOut(function(error){
				if(error){
					return showError(error);
				}
			});
		};



		/**
		 * 
		 */
//		DropboxConnector.prototype.checkAuthentication = function(){
//			console.log("DropboxAPI -> checkAuthentication called");
//			this.client.authenticate({interactive: false}, function(error, client) {
//				if (error) {
//					return showError(error);
//				}
//				if (client.isAuthenticated()) {
//
//					// Cached credentials are available, make Dropbox API calls.
//					//getUserInfos(client);
//					//doSomethingCool(client);
//					setButtonVisible("#dropbox_openFile");
//				} else {
//
//					// show and set up the "Sign into Dropbox" button
//					//setButtonVisible("#dropbox_signin");
//					//button.setAttribute("class", "visible");
////					button.addEventListener("click", function() {
//////					// The user will have to click an 'Authorize' button.
//////					client.authenticate(function(error, client) {
//////					if (error) {
//////					return showError(error);
//////					}
//
//////					//user authenticated
//////					alert('user authenticated2 pass 2');
//////					//getUserInfos(client);
//////					//doSomethingCool(client);
//
//////					});
////					authenticateClient();
////					});
//				}
//			});	 
//		}
		


		/*------------------- private FONCTIONS --------------*/



		var showError = function(error) {
			switch (error.status) {
			case Dropbox.ApiError.INVALID_TOKEN:
				// If you're using dropbox.js, the only cause behind this error is that
				// the user token expired.
				// Get the user through the authentication flow again.
				break;

			case Dropbox.ApiError.NOT_FOUND:
				// The file or folder you tried to access is not in the user's Dropbox.
				// Handling this error is specific to your application.
				break;

			case Dropbox.ApiError.OVER_QUOTA:
				// The user is over their Dropbox quota.
				// Tell them their Dropbox is full. Refreshing the page won't help.
				break;

			case Dropbox.ApiError.RATE_LIMITED:
				// Too many API requests. Tell the user to try again later.
				// Long-term, optimize your code to use fewer API calls.
				break;

			case Dropbox.ApiError.NETWORK_ERROR:
				// An error occurred at the XMLHttpRequest layer.
				// Most likely, the user's network connection is down.
				// API calls will not succeed until the user gets back online.
				break;

			case Dropbox.ApiError.INVALID_PARAM:
			case Dropbox.ApiError.OAUTH_ERROR:
			case Dropbox.ApiError.INVALID_METHOD:
			default:
				// Caused by a bug in dropbox.js, in your application, or in Dropbox.
				// Tell the user an error occurred, ask them to refresh the page.
			}
		};



	}

}