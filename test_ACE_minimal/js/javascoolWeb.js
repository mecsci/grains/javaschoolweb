
var APPLICATION_MODE = {
		ONLINE: "online",
		OFFLINE: "offline"
};



function generateUniqueID(){
	var uniqueID = new Date();
	var myRandom=Math.floor(Math.random()*1000)
	return uniqueID.getTime()+''+myRandom;
}

/**
 * the key in the local storage browser 
 */
var storageKey_file = "javascoolWeb_file";
var storageKey_settings = "javascoolWeb_settings";
var storageKey_application = "javascoolWeb_application";

function createData(filename, content, fileId){
	if(fileId == null){
		fileId = generateUniqueID();
	}
	return {id:fileId, name:filename, content:content};
}


function getIdFileInTab(tab, fileID){
	var result = null;
	for (var i=0; i<tab.length; i++){
		if(tab[i].id == fileID){
			result = i;
			break;
		}
	}

	return result;
}


function Settings(){
	this.dropboxIsEnabled = false;
	this.onedriveIsEnabled = false;
	this.googledrivIsEnabled = false;

//	if ( typeof Settings.initialized == "undefined" ) {
//
//
//		/**
//		 * private function used to enable the dropbox plugin/functionalities
//		 */
//		var enableDropboxPlugin = function(){
//			console.log("Settings  enabling dropbox plugin");
//			this.dropboxIsEnabled = true;
//		};
//
//
//		/**
//		 * private function used to disable the dropbox plugin/functionalities
//		 */
//		var disableDropboxPlugin = function(){
//			console.log("Settings disabling dropbox plugin");
//			this.dropboxIsEnabled = false;
//		};
//		
//		
//
//
//		/**
//		 * private function used to enable the Microsoft OneDrive plugin/functionalities
//		 */
//		var enableOnedrivePlugin = function(){
//			console.log("Settings  enabling Onedrive plugin");
//			this.onedriveIsEnabled = true;
//		};
//
//		/**
//		 * private function used to disable the Microsoft OneDrive plugin/functionalities
//		 */
//		var disableOnedrivePlugin = function(){
//			console.log("Settings disabling Onedrive plugin");
//			this.onedriveIsEnabled = false;
//		};
//
//
//		/**
//		 * private function used to enable the Google Drive plugin/functionalities
//		 */
//		var enableGoogledrivePlugin = function(){
//			console.log("Settings  enabling Google Drive plugin");
//			this.googledrivIsEnabled = true;
//		};
//
//		/**
//		 * private function used to disable the Google Drive plugin/functionalities
//		 */
//		var disableGoogledrivePlugin = function(){
//			console.log("Settings disabling Google Drive plugin");
//			this.googledrivIsEnabled = false;
//		};
//
//
//
//		/**
//		 * Permits to change the enable/disble the Dropbox plugin
//		 * @param active true to active the plugin, false to disable the plugin
//		 */
//		Settings.prototype.setDropboxPluginStatus  = function(active){
//			if(active){
//				enableDropboxPlugin();
//			}else{
//				disableDropboxPlugin();
//			}
//		};
//		
//		
//		
//
//		/**
//		 * Permits to change the enable/disble the Microsoft OneDrive plugin
//		 * @param active true to active the plugin, false to disable the plugin
//		 */
//		Settings.prototype.setOnedrivePluginStatus  = function(active){
//			if(active){
//				enableOnedrivePlugin();
//			}else{
//				disableOnedrivePlugin();
//			}
//		};
//
//
//		/**
//		 * Permits to change the enable/disble the Google Drive plugin
//		 * @param active true to active the plugin, false to disable the plugin
//		 */
//		Settings.prototype.setGoogledrivePluginStatus  = function(active){
//			if(active){
//				enableGoogledrivePlugin();
//			}else{
//				disableGoogledrivePlugin();
//			}
//		};
//
//	}
};




function JavascoolApplication(){

	//this.mode = APPLICATION_MODE.OFFLINE;
	this.modeOffline = true;
	this.settings = new Settings();
	this.localFiles = [];
	this.dropboxFiles = [];
	
//TODO	
	this.dropbox = new DropboxConnector('aeflu0hhf6nt977');
	this.dropbox.init();
	this.dropboxClientIsAuthenticated = this.dropbox.verifyClientIsAuthenticated();
	
	var APP_CLIENT_ID = "0000000044146805";
	//var REDIRECT_URL = "https://login.live.com/oauth20_desktop.srf";
	var REDIRECT_URL = "http://site-local.dev/git/javascoolWeb/test_ACE_minimal/ide.html";
	
	this.oneDrive = null;new OneDriveConnector(APP_CLIENT_ID, REDIRECT_URL);
	
	this.currentFile = null;

	if ( typeof JavascoolApplication.initialized == "undefined" ) {



		/**
		 * private function used to set the application mode to offline
		 */
		var setModeOffline = function(){
			console.log("Javascool appication set to offline mode");
			//$("#settings").hide();
		};


		/**
		 * private function used to set the application mode to online
		 */
		var setModeOnline = function(){
			console.log("Javascool appication set to online mode");
			//$("#settings").show();
		};




		/**
		 * Permits to set the application mode from offline/online to online/offline
		 */
		JavascoolApplication.prototype.setMode  = function(newMode){
			if(newMode){//case switch to online
				setModeOnline();
			}else{//case swith to offline
				setModeOffline();
			}

		};

		/**
		 * Permits to get the application settings
		 */
		JavascoolApplication.prototype.getSettings  = function(){
			return this.settings;
		};


	}

};



/*===================== GLOBAL CONSTANTS ============================*/


var navigator_enum = {
		CHROME: "Chrome",
		OPERA: "Opera",
		FIREFOX: "Firefox",
		IE: "MSIE",
		UNKNOW: "Unknow"
};


/*===================================================================*/
/**
 * 
 *
 */
function downloadURI(uri, name) {
	var link = document.createElement("a");
	link.download = name;
	link.href = uri;
	link.click();
}



/**
 * return the browser type
 */
function getBrowser() { 
	var browser = navigator_enum.UNKNOW;

	if(navigator.userAgent.indexOf(navigator_enum.CHROME) != -1 ) {
		browser = navigator_enum.CHROME;
	}
	else if(navigator.userAgent.indexOf(navigator_enum.OPERA) != -1 ){
		browser = navigator_enum.OPERA;
	}
	else if(navigator.userAgent.indexOf(navigator_enum.FIREFOX) != -1 ) {
		browser = navigator_enum.FIREFOX;
	}
	else if((navigator.userAgent.indexOf(navigator_enum.IE) != -1 ) || (!!document.documentMode == true /*IF IE > 10*/)) {
		browser = navigator_enum.IE;
	}

	return browser;
}

//function appendAnnotations(annots){

//var message="";
//for (var key in annots){
//if (annots.hasOwnProperty(key))

//message = message.concat("ligne : "+(annots[key].row+1)+" -> "+annots[key].text+"\n");
//}

//$("#errors").val(message);
//}


/**
 * permits to manage include tag in the webpage
 */
function manageIncludes(){
	$( "include" ).each(function() {
		console.log($(this).attr('src'));
		$(this).load($(this).attr('src'));
	}); 	
}







/**
 * This fucntion permit to save a file with the desired content on the disk
 * @param filename the file name to register on the disk
 * @param content the content of the file to save
 */
function downloadFile(filename, content){
	console.log("saving file : "+filename+" on the disk");
	var textToSave = editor.getValue();
	var textFileAsBlob = new Blob([content], {type:'application/javascript'});
	var downloadLink = document.createElement("a");
	downloadLink.download = filename;
	downloadLink.innerHTML = "Download File";
	if (window.webkitURL != null){
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
	}else{
		// Firefox requires the link to be added to the DOM
		// before it can be clicked.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}

	downloadLink.click();
}



/**
 * permit to load the file content in the editor
 */
function openFile(file, callback){
	console.log(file);
	var reader = new FileReader();
	
	reader.onload = function(e) {
		callback(reader.result);
	}

	reader.readAsText(file);
}

