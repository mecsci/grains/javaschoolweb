var app = angular.module('javascoolWebApplication', [ 'angularTreeview' ]);

app.factory('MyApplication', function() {
	return new JavascoolApplication();
});

app.factory('BrowserStorage', function() {
	return new BrowserStorage();
});

app.factory('dropboxFiles', function() {
	return [];
});

// ----------directives ------------------

app
		.directive(
				'toggleButtonMode',
				function($filter) {
					return {
						restrict : 'A',
						replace : true,
						transclude : true,
						require : 'ngModel',
						scope : {
							'ngModel' : '='
						},
						template : '<button type="button" class="btn ng-class:{\'btn-info\':ngModel, \'btn-success\':!ngModel}" data-toggle="toggle" ng-click="ngModel = !ngModel" ng-transclude></button>'
					};
				});

app
		.directive(
				'toggleButtonSettings',
				function($filter) {
					return {
						restrict : 'A',
						replace : true,
						transclude : true,
						require : 'ngModel',
						scope : {
							'ngModel' : '='
						},
						template : '<button type="button" class="btn btn-block ng-class:{\'btn-success\':ngModel, \'btn-danger\':!ngModel}" data-toggle="toggle" ng-click="ngModel = !ngModel" ng-transclude></button>'
					};
				});


// ----------controllers ------------------

/*
 * TODO to deplace in an API extract the fodler names of the proglet from q html
 * content @return the list of of folder names
 */
function extractProgletFolderName(htmlString) {

	var foldersName = [];

	var firstDir = htmlString.indexOf("[DIR]");// index of the first directory
	// the first directoty is corresponding to the parent folder, so we skpt it
	var index = htmlString.indexOf("[DIR]", firstDir + 7);

	while (index != -1) {
		var a = htmlString.indexOf("href=", index);
		var b = htmlString.indexOf(">", a);
		var forlderName = htmlString.substring(a + 6, b - 2);
		foldersName.push(forlderName);
		index = htmlString.indexOf("[DIR]", index + 7);

	}

	return foldersName.sort();
}

/**
 * the proglet activities controller
 */
app.controller('ProgletsController', function($scope, $http, $location,
		MyApplication) {

	$http.get("./proglets/proglets.json").success(
			function(progletsJsonFile) {
				var proglets = [];
				
				for (var i = 0; i < progletsJsonFile.proglets.length; i++) {
					$http.get("./proglets/" + progletsJsonFile.proglets[i] + "/proglet.json")
							.success(function(jsonFile) {
								proglets.push(jsonFile);
							});
				}

				$scope.proglets = proglets;

			});

	/**
	 * store the activity selected in the current application if the parameter
	 * is null the current activity will be setted to null.
	 */
	$scope.openActivite = function(activity) {
		MyApplication.activity = activity;
	};

});

/**
 * the main controller controller for the website application
 */
app.controller('ApplicationController', function($scope, MyApplication,
		BrowserStorage, dropboxFiles) {

	$scope.application = MyApplication;
	$scope.localFiles = dropboxFiles;
	BrowserStorage.checkStorageFunctionalities();
	// cleaning the storage
	BrowserStorage.deleteSessionData();
	// BrowserStorage.deleteLocalData();

	// reading previous application saved
	var applicationSaved = BrowserStorage
			.getSessionData(storageKey_application);
	if (applicationSaved != null) {
		MyApplication = applicationSaved;
	}

	// reading previous settings saved
	var settings = BrowserStorage.getLocalData(storageKey_settings);
	if (settings != null) {
		MyApplication.settings = settings;
	}

	// TODO uncomment this process when i could get editor functionnalities are
	// done
	var dataStored = BrowserStorage.getSessionData();
	if (dataStored != null) {
		MyApplication.localFiles[0] = dataStored;
	}

	$scope.log = function(message) {
		console.log(message);
	};

	$scope.saveSettings = function() {
		BrowserStorage.saveLocalData(MyApplication.settings,
				storageKey_settings, true);
	}

});

/**
 * OneDrive controller
 */
app
		.controller(
				'OneDriveFilesControl',
				function($scope, $http, $timeout, MyApplication) {

					var oneDrive = MyApplication.oneDrive;
					// TODO oneDrive.init();
					//oneDrive.checkSessionActive();
					//oneDrive.signInUser();

					$scope.searchFiles = function(node) {
						console.log("searchFiles called : " + node);
						if (node != null && node.isFolder && node.items == null) {
							oneDrive.listFolders(node.id, function(a) {
								$scope.$apply(function() {
									node.items = a;
								});

							});
						}
					};

//TODO uncomment					
//					oneDrive.listFolders(null, function(datas) {
//						console.log(datas);
//						$scope.files = datas;
//					});

					/**
					 * login user to onedrive
					 */
					$scope.login = function() {
						oneDrive.signInUser();
					};

					/**
					 * logout user from oneDrive
					 */
					$scope.logout = function() {
						oneDrive.signOutUser();
					};

					$scope.refresh = function() {
						oneDrive.listFolders();
					};

					// -------- test click dbclick

					$scope.singleClick = function(node) {
						if ($scope.clicked) {
							$scope.cancelClick = true;
							return;
						}

						$scope.clicked = true;

						$timeout(function() {
							if ($scope.cancelClick) {
								$scope.cancelClick = false;
								$scope.clicked = false;
								return;
							}

							// do something with your single click here
							$scope.searchFiles(node);

							// clean up
							$scope.cancelClick = false;
							$scope.clicked = false;
						}, 500);
					};

					$scope.doubleClick = function(item) {

						$timeout(function() {

							// do something with your double click here
							console.log(" double click !!!!!");
							if (item != null) {
								if (item.isFile) {

									// dropbox.readFile(item.path, function(e){
									// editor.setValue(e);
									// });
									oneDrive
											.readFile(
													item.id,
													function(location) {
														console
																.log("hhtp function : "
																		+ location);

														$http(
																{
																	method : 'GET',
																	url : location,
																	headers : {
																		'Access-Control-Allow-Origin' : '*',
																		'Access-Control-Allow-Methods' : 'GET, POST, PUT, DELETE, OPTIONS',
																		'Access-Control-Allow-Headers' : 'Content-Type, X-Requested-With',
																	}
																})
																.success(
																		function(
																				d) {
																			console
																					.log("yay");
																		})
																.error(
																		function(
																				d) {
																			console
																					.log("nope");
																		});

														// $http.get(location).
														// success(function(data,
														// status, headers,
														// config) {
														// // this callback will
														// be called
														// asynchronously
														// // when the response
														// is available
														// console.log("http
														// success !!!");
														// }).
														// error(function(data,
														// status, , config) {
														// // called
														// asynchronously if an
														// error occurs
														// // or server returns
														// response with an
														// error status.
														// console.log("http
														// error");
														// console.log(data);
														// console.log(status);
														// console.log(headers);
														// console.log(config);
														// });
													});

								} else {
									$scope.searchFiles(item);
								}
							}
						});
					};

				});

app.controller('LocalFilesControlOpen', function($scope, $timeout,
		MyApplication, BrowserStorage) {

	/**
	 * open a file, update the applicatoin files list and save the file
	 * informations in localStorage
	 */
	$scope.file_changed = function(element) {

		console.log("file_changed called !!!!!!");
		$scope.$apply(function(scope) {
			var id = null;
			openFile(element.files[0], function(content) {
				editor.setValue(content);
				var data = createData(element.files[0].name, content);
				id = data.id;
				BrowserStorage.saveSessionData(data, storageKey_file, true);
			});

			// TODO probleme possible avec l'id different en base en en locale
			// TODO BUG !!!!!!!!! l(id en base n'est pas le meme que dans la
			// page
			var data = createData(element.files[0].name, null, id);// TODO fix
			// null
			// parameter
			// to true
			// value,
			// null
			// because
			// we need
			// to wait
			// the
			// result
			// from
			// dropbox
			// request
			// outside
			// the
			// callback
			$scope.files = MyApplication.localFiles.push(data);
			MyApplication.currentFile = data;

		});
	};
});

app
		.controller('LocalFilesControlSave',
				function($scope, $timeout, MyApplication, BrowserStorage) {

					/**
					 * permits to save
					 */
					$scope.saveFile = function() {
						// bug ici car le fichier courant n'est pas forcemment
						// le fichier en session !!!!!
						// donc si ou onvre un fichier via l'arbre et qu'on le
						// modifie c'est le fichier en base local qui est
						// modifié.
						var dataStored = BrowserStorage
								.getSessionData(storageKey_file);
						var data = createData(dataStored.name, editor
								.getValue(), dataStored.id);
						var dataUpdated = BrowserStorage.saveSessionData(data,
								storageKey_file, true);
						var index = getIdFileInTab(MyApplication.localFiles,
								dataUpdated.id);
						if (index != null) {
							MyApplication.localFiles[index] = dataUpdated;
						} else {
							throw new Error("can't found the file with id : "
									+ dataUpdated.id
									+ " inside the application datas");
						}
						MyApplication.currentFile = dataUpdated;

					};

				});

app.controller('LocalFilesControlDownload', function($scope, $timeout,
		MyApplication, BrowserStorage) {

	/**
	 * permits to download file
	 */
	$scope.download = function() {
		console.log("download function called !!!!");
		var dataStored = BrowserStorage.getSessionData(storageKey_file);
		downloadFile(dataStored.name, dataStored.content);
	}

});

app.controller('LocalFilesControlSaveAs', function($scope, $timeout,
		MyApplication, BrowserStorage) {

	/**
	 * permits to save file with filenameSaveAS for filename
	 */
	$scope.saveFileAs = function() {

		var data = createData($scope.filenameSaveAS, editor.getValue());

		var dataStored = BrowserStorage.saveSessionData(data, storageKey_file,
				true);
		console.log("sebastien local store : ");
		console.log(dataStored);
		MyApplication.localFiles.push(dataStored);
		MyApplication.currentFile = dataStored;
	};

});

/**
 * local file controller
 */
app.controller('LocalFilesControl', function($scope, $timeout, MyApplication,
		BrowserStorage) {

	$scope.files = MyApplication.localFiles;
	$scope.saveFileIsDisabled = true;
	$scope.downloadFileIsDisabled = true;
	$scope.filenameSaveAS = undefined;

	/**
	 * refreqsh the explorer view
	 */
	$scope.refresh = function() {
		var dataStored = BrowserStorage.getSessionData(storageKey_file);
		MyApplication.localFiles[0] = dataStored; // TODO only correct for one
		// file
	};

	/**
	 * local file tree view simple click action definition
	 */
	$scope.singleClick = function(node) {
		if ($scope.clicked) {
			$scope.cancelClick = true;
			return;
		}

		$scope.clicked = true;

		$timeout(function() {
			if ($scope.cancelClick) {
				$scope.cancelClick = false;
				$scope.clicked = false;
				return;
			}

			// do something with your single click here
			console.log("local tree view simple click action to define!!");

			// clean up
			$scope.cancelClick = false;
			$scope.clicked = false;
		}, 500);
	};

	/**
	 * local files tree view double click action definition
	 */
	$scope.doubleClick = function(item) {

		$timeout(function() {

			// do something with your double click here
			if (item != null) {

				editor.setValue(item.content);
				MyApplication.currentFile = item;
			}
		});
	};

});

/**
 * dropbox controller
 */

app.controller('DropboxFilesControlConnection', function($scope, $timeout,
		MyApplication, dropboxFiles) {

	var dropbox = MyApplication.dropbox;

	/**
	 * login the user to access to his dropox content
	 */
	$scope.login = function() {
		dropbox.authenticateClient();
		MyApplication.dropboxClientIsAuthenticated = dropbox
				.verifyClientIsAuthenticated();
		console.log(MyApplication);
	};

});

app.controller('DropboxFilesControlDisconnection', function($scope, $timeout,
		MyApplication, dropboxFiles) {

	var dropbox = MyApplication.dropbox;

	/**
	 * logout user from dropbox
	 */
	$scope.logout = function() {
		dropbox.disconnectClient();
		MyApplication.dropboxClientIsAuthenticated = dropbox
				.verifyClientIsAuthenticated();
		console.log(MyApplication);
		console.log(dropbox.verifyClientIsAuthenticated());
	};
});

app.controller('DropboxFilesControl', function($scope, $timeout, MyApplication,
		dropboxFiles) {

	var dropbox = MyApplication.dropbox;
	$scope.files = dropboxFiles;

	// $scope.sinon = function(item) {
	// console.log(item);
	// if(item != null && item.isFolder && item.items == null){
	// dropbox.listContent(item.path, function(a){
	// $scope.$apply(function () {
	// item.items=a;
	// });
	//
	// });
	// }else{
	// if(item !=null && item.isFile){
	// dropbox.readFile(item.path, function(e){
	//
	// editor.setValue(e);
	//
	// });
	// }
	// }
	// };

	// -----------
	/**
	 * this function permit to serch the content of a specified folder in
	 * dropbox (http request)
	 * 
	 * @param node
	 *            an object who is presenting a dropbox folder or file
	 */
	$scope.searchFiles = function(node) {
		console.log("searchFiles called : " + node);
		if (node != null && node.isFolder && node.items == null) {
			dropbox.listContent(node.path, function(a) {
				$scope.$apply(function() {
					node.items = a;
				});

			});
		}
	};

	/**
	 * call to refresh the view
	 */
	$scope.refresh = function() {
		console.log("refresh function called !!!!");
		dropbox.listContent("/", function(a) {
			$scope.$apply(function() {
				dropboxFiles = a;
				$scope.files = dropboxFiles;
			});

		});
	};

	$scope.getItems = function() {
		alert("test controller getItems function !!!");
		$scope.refresh();
		console.log("end getItems fonction ")
	};

	// -------- test click dbclick

	$scope.singleClick = function(node) {
		if ($scope.clicked) {
			$scope.cancelClick = true;
			return;
		}

		$scope.clicked = true;

		$timeout(function() {
			if ($scope.cancelClick) {
				$scope.cancelClick = false;
				$scope.clicked = false;
				return;
			}

			// do something with your single click here
			$scope.searchFiles(node);

			// clean up
			$scope.cancelClick = false;
			$scope.clicked = false;
		}, 500);
	};

	$scope.doubleClick = function(item) {

		$timeout(function() {

			// do something with your double click here
			console.log(" double click !!!!!");
			if (item != null) {
				if (item.isFile) {

					dropbox.readFile(item.path, function(e) {
						editor.setValue(e);
					});

				} else {
					$scope.searchFiles(item);
				}
			}
		});
	};

});
