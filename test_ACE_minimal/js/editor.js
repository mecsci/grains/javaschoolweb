/**
 * this file contains all the functions usefull to manipulate an ACE editor.
 * 
 */


/**
 * this contains all the different type of annotations that we can found in a ACE editor.
 * the index value correspond to the index of this type of annotations in the result of the
 * method extractCodeAnnotations.
 * the value correspond to the name of this type of annotation.
 */
var enum_annotation = {
		ERROR : {index: 0, value: "error"}, 
		WARNING: {index: 1, value: "warning"}, 
		INFO : {index: 2, value: "info"}
};

/**
 * Permits to extract the annotations from the specified editor.
 * three type of annotations are extracted in differents list: errors, warning, infos
 * 
 * @param editor the ACE editor
 * @returns {Array} an array of annotations
 */
function extractCodeAnnotations(editor){
	var list_errors = [];
	var list_warnings = [];
	var list_infos = [];

	var annots = editor.getSession().getAnnotations();

	for (var key in annots){
		if (annots.hasOwnProperty(key)){
			switch(annots[key].type){

			case enum_annotation.ERROR.value : {
				list_errors.push(annots[key]);
				break;
			}
			case enum_annotation.WARNING.value: { 
				list_warnings.push(annots[key]); break;
			}
			case enum_annotation.INFO.value:	{
				list_infos.push(annots[key]); break;
			}
			}

		}
	}

	return [list_errors, list_warnings, list_infos];
}


/**
 * permits to check if the list contains errors annotations
 * 
 * @param list the list to check, this list correpond to the result of the function extractCodeAnnotations(...)
 * @returns {Boolean} true if the list contains errors
 */
function haveErrors(list){
	return list[enum_annotation.ERROR.index].length > 0;
}


/**
 * permits to get errors annotations extracted by the method extractCodeAnnotations
 * 
 * @param the annotations list extracted by the method extractCodeAnnotations
 * @returns list the erros annotations list
 */
function getErrorsAnnotations(list){
	return getAnnotations(list, enum_annotation.ERROR);
}

/**
 * permits to get warnings annotations extracted by the method extractCodeAnnotations
 * 
 * @param the annotations list extracted by the method extractCodeAnnotations
 * @returns list the warnings annotations list
 */
function getWarningsAnnotations(list){
	return getAnnotations(list, enum_annotation.WARNING);
}


/**
 * permits to get informations annotations extracted by the method extractCodeAnnotations
 * 
 * @param the annotations list extracted by the method extractCodeAnnotations
 * @returns list the info annotations list
 */
function getInfosAnnotations(list){
	return getAnnotations(list, enum_annotation.INFO);
}



/**
 * permit to get a type of annotations from the annotations extracted by the method extractCodeAnnotations
 * 
 * @param list he annotations list extracted by the method extractCodeAnnotations
 * @param enumError the type of annotations that we want
 * @returns the list of annotations matching the type in parameter
 */
function getAnnotations(list, enumError){
	return list[enumError.index];
}


