function transmitErrors(e){
	//var info = prompt(e);
	window.parent.printErrors(e);
}

function getColumnIndex(index, exception){

	var stack = exception.stack;
	var stackLineInfo = stack.search(/[0-9]+:[0-9]+/);
	var stackColumnInfo = stack.indexOf(":", stackLineInfo);
	var indexColumn = stack.substring(stackColumnInfo+1, stack.length);
	return Number(indexColumn - index);
}

function extractLineError(column, code){
	var codeString = String(code);
	var s = codeString.substring(column, codeString.length);
	var i = s.search(/@JVSL:[0-9]+@/);
	var u = s.substring(i+1,s.length);
	var ligne = u.substring(u.indexOf(":")+1,u.indexOf("@"));
	return Number(ligne);
}

function printError(index, exception, code){
	var ligne = extractLineError(getColumnIndex(index, exception), code);
	alert("Ligne : "+ligne+" -> "+exception);
}


/** 
 this file contains all the function used by the proglet : 

 -	abcdAlgos  
 -	
 -	
 -	

 */



function println(data){

	document.writeln(data+"<br/>");
}




function readInteger(message){

	return Number(read(message));
}



/**
 * 
 * @param message
 * @returns
 */
function readBoolean(message){
	var result;
	var b = read(message);
	if(b == "true") {
		result = true;
	}
	if(b == "false") {
		result = false;
	}
	
	return result;
}


function readString(meesage){
	return String(read(meesage));
}



//TODO will be better tu used setTimeOut
function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds){
			break;
		}
	}
}


/**
 * PRIVATE function
 * permit ro read a user information
 * @param message the message to print 
 * @returns the text entered by the user
 */
function read(message){
	return prompt(message);
}


function clear(){
	$('body').text("");
}


function equals(a,b){

	return a === b;
}


function max(args){
	return Math.max(args); //TODO doesn't work
}


function random(a,b){
	return Math.floor((Math.random() * b) + a);
}

