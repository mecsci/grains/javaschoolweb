var app = angular.module('proglets', []);

app.factory('MyApplication', function() {
	return new JavascoolApplication();
});

app.factory('BrowserStorage', function() {
	return new BrowserStorage();
});

// ----------controllers ------------------



app.controller('ProgletsController', function($scope, $http, $location) {

	$http.get("./proglets/").success(
			function(response) {
				var folders = extractProgletFolderName(response);

				var proglets = [];

				for (var i = 0; i < folders.length; i++) {
					$http.get("./proglets/" + folders[i] + "/proglet.json")
							.success(function(jsonFile) {
								console.log(jsonFile);
								proglets.push(jsonFile);
							});
				}

				$scope.proglets = proglets;
				$progletSize = proglets.length;
			});

	
	$http.get("./proglets/proglets.json").success(function(response) {
		// te $scope.proglets = response.proglets;
	});

	$scope.openActivite = function(activite) {
		console.log(activite);
		$location.path("?act=seb");
	};

});
