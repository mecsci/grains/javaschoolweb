/*
	@license Angular Treeview version 0.1.6
	ⓒ 2013 AHN JAE-HA http://github.com/eu81273/angular.treeview
	License: MIT


	[TREE attribute]
	angular-treeview: the treeview directive
	tree-id : each tree's unique id.
	tree-model : the tree model on $scope.
	node-id : each node's id
	node-label : each node's label
	node-children: each node's children

	<div
		data-angular-treeview="true"
		data-tree-id="tree"
		data-tree-model="roleList"
		data-node-id="roleId"
		data-node-label="roleName"
		data-node-children="children" >
	</div>
*/

(function ( angular ) {
	'use strict';

	angular.module( 'angularTreeview', [] ).directive( 'treeModel', ['$compile', function( $compile ) {
		return {
			restrict: 'A',
			link: function ( scope, element, attrs ) {
				//tree id
				var treeId = attrs.treeId;
			
				//tree model
				var treeModel = attrs.treeModel;

				//node id
				var nodeId = attrs.nodeId || 'id';

				//node label
				var nodeLabel = attrs.nodeLabel || 'label';

				//children
				var nodeChildren = attrs.nodeChildren || 'children';

				var nodeIsFolder = attrs.nodeIsFolder || 'isFolder';
				
//				<span ng-click="searchFiles($index)"><i ng-class="{{file.isFolder}} ? 'icon-folder-close' : 'icon-leaf'"
//					></i>&emsp;{{file.name}}</span>
//				
				
				
				//tree template
				var template2 =
					'<ul>' +
						'<li data-ng-repeat="node in ' + treeModel + '">' +
							'<i class="collapsed" data-ng-show="node.' + nodeChildren + '.length && node.collapsed" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
							'<i class="expanded" data-ng-show="node.' + nodeChildren + '.length && !node.collapsed" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
							'<i class="normal" data-ng-hide="node.' + nodeChildren + '.length"></i> ' +
							'<span data-ng-class="node.selected"  data-ng-click="' + treeId + '.selectNodeLabel(node)">{{node.' + nodeLabel + '}}</span>' +
							'<div data-ng-hide="node.collapsed" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '></div>' +
						'</li>' +
					'</ul>';


				
				//tree template
				var template3 =
					'<ul>' +
						'<li data-ng-repeat="node in ' + treeModel + '">' +
						'<i class="expanded" data-ng-show="node.'+nodeIsFolder+'  && node.collapsed" data-ng-click="searchFiles(node); ' + treeId + '.selectNodeHead(node)"></i>' +
						'<i class="collapsed" data-ng-show="node.'+nodeIsFolder+' && !node.collapsed" data-ng-click="searchFiles(node); ' + treeId + '.selectNodeHead(node)"></i>' +
						'<i class="normal" data-ng-hide="node.' + nodeIsFolder +'"></i> ' +
							'<span data-ng-class="node.selected" data-ng-click="sinon(node); ' + treeId + '.selectNodeLabel(node)">{{node.' + nodeLabel + '}}</span>' +
							'<div data-ng-hide="node.collapsed" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '></div>' +
						'</li>' +
					'</ul>';



			
				var template4 =
					'<ul>' +
						'<li data-ng-repeat="node in ' + treeModel + '">' +
						'<i class="open" data-ng-show="node.'+nodeIsFolder+'  && !node.reduced" data-ng-click="searchFiles(node); ' + treeId + '.selectNodeHead(node)"></i>' +
						'<i class="closed" data-ng-show="node.'+nodeIsFolder+' && node.reduced" data-ng-click="searchFiles(node); ' + treeId + '.selectNodeHead(node)"></i>' +
						'<i class="normal" data-ng-hide="node.' + nodeIsFolder +'"></i> ' +
							'<span data-ng-class="node.selected" ng-dblclick="doubleClick(node)" data-ng-click="singleClick(node); ' + treeId + '.selectNodeLabel(node)">{{node.' + nodeLabel + '}}</span>' +
							'<div data-ng-show="node.expanded" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '></div>' +
						'</li>' +
					'</ul>';
				
				
				var template =
					'<ul>' +
						'<li data-ng-repeat="node in ' + treeModel + '">' +
						'<i class="glyphicon glyphicon-expand" data-ng-show="!node.expanded && node.'+nodeIsFolder+'" data-ng-click="searchFiles(node); ' + treeId + '.selectNodeHead(node)"></i>'+
						'<i class="glyphicon glyphicon-collapse-down" data-ng-show="node.expanded && node.'+nodeIsFolder+'" data-ng-click="searchFiles(node); ' + treeId + '.selectNodeHead(node)"></i>'+
						
						
						'<i data-ng-class="{\'open \': node.'+nodeIsFolder+', \'normal\': !node.'+nodeIsFolder+'}"></i> ' +
							'<span data-ng-class="node.selected" data-ng-dblclick="doubleClick(node)" data-ng-click="singleClick(node); ' + treeId + '.selectNodeLabel(node)">{{node.' + nodeLabel + '}}</span>' +
							'<div data-ng-show="node.expanded" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '></div>' +
						'</li>' +
					'</ul>';
				
				//check tree id, tree model
				if( treeId && treeModel ) {

					//root node
					if( attrs.angularTreeview ) {
					
						//create tree object if not exists
						scope[treeId] = scope[treeId] || {};

						//if node head clicks,
						scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function( selectedNode ){

							console.log('selectNodeHead called');
							
							//Collapse or Expand				
							if(selectedNode.expanded == undefined){
								selectedNode.expanded=false;
							}
							selectedNode.expanded = !selectedNode.expanded;
						};

						//if node label clicks,
						scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function( selectedNode ){

							//remove highlight from previous node
							if( scope[treeId].currentNode && scope[treeId].currentNode.selected ) {
								scope[treeId].currentNode.selected = undefined;
							}

							//set highlight to selected node
							selectedNode.selected = 'selected';

							//set currentNode
							scope[treeId].currentNode = selectedNode;
						};
					}

					//Rendering template.
					element.html('').append( $compile( template )( scope ) );
				}
			}
		};
	}]);
})( angular );
