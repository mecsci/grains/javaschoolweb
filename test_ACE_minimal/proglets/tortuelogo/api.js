/** Définit les fonctions de la proglet qui permet de simuler la tortue logo.
 *
 * @see <a href="http://fr.wikipedia.org/wiki/Logo_(langage)#Primitives_graphiques">La référence du langage logo</a>
 * @see <a href="Functions.java.html">code source</a>
 * @serial exclude
 */



/*=============================================================*/
/*				PRIVATE FUNCTIONS							   */
/*=============================================================*/

//$(document).ready(function() {


var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var width = canvas.width;
var height = canvas.height;


var canvas_x = 0, canvas_y = 0; //double
var angle = 0; //double
var pen_color ;//= Color.BLACK; //Color java class
var pen = true; //boolean
var turtle_shown = true; //boolean
var imageObj = new Image();
//imageObj.onload = function() {
//ctx.drawImage(imageObj, 69, 50);
//};
imageObj.src = './proglets/tortueLogo/turtle.gif';



//var colors = [ Color.BLACK, new Color(150, 75, 0), Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE, Color.MAGENTA, Color.GRAY, Color.WHITE };


/**
 * PRIVATE function
 * Updates the turtle position and draw if required
 * 
 */
function update(x, y) {
	console.log("update -> I'm moving from : ("+canvas_x+","+canvas_y+") to : ("+x+","+y+")");
	if(x < 0) {
		x = 0;
	}
	if(x > width) {
		x = width;
	}
	if(y < 0) {
		y = 0;
	}
	if(y > height) {
		y = height;
	}



	if(pen) {
		draw(canvas_x, x, canvas_y, y);
	}

	canvas_x = x;
	canvas_y = y;
	//ctx.moveTo(canvas_x,canvas_y);

	//if (turtle_shown) {
	showTurtle(x, y);
		//ctx.show(x, y,turtle_shown); TODO to translate in javascript
		//sleep(1);
	//}
}


/**
 * PRIVATE function
 * draw a line form point(x1,y1) to (x2,y2)
 * @param x1
 * @param x2
 * @param y1
 * @param y2
 * @returns
 */
function  draw(x1, x2, y1, y2) {
	//setTimeout(function(){}, 1000);
	console.log("i'm drawing : from ("+x1+","+y1+") to ("+x2+","+y2+")");
	if(Math.abs(x1 - x2) > Math.abs(y1 - y2)) {
		if(x1 < x2) {
			draw_x(x1, x2, y1, y2);
		} else if(x1 > x2) {
			draw_x(x2, x1, y2, y1);
		}
	} else {
		if(y1 < y2) {
			draw_y(x1, x2, y1, y2);
		} else if(y1 > y2) {
			draw_y(x2, x1, y2, y1);
		}
	}
}


/**
 * PRIVATE function
 * draw a line from the point (x1,y1) to (x2,y2)
 * @param x1
 * @param x2
 * @param y1
 * @param y2
 */
function draw_x(x1, x2, y1, y2) {
	console.log("draw_x-> i'm drawing x : from ("+x1+","+y1+") to ("+x2+","+y2+")");

	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.strokeStyle = pen_color;
	ctx.stroke();


//	for(var x = x1; x <= x2; x++){
//	//TODO to translate in javascript
//	//ctx.add(x, y1 + ((y2 - y1) * (x - x1)) / (x2 - x1), pen_color);
//	ctx.beginPath();
//	ctx.lineTo(x, y1 + ((y2 - y1) * (x - x1)) / (x2 - x1));
//	ctx.moveTo(x, y1 + ((y2 - y1) * (x - x1)) / (x2 - x1));
//	// ctx.lineTo(x2, y2);
//	ctx.stroke();
//	}
}


/**
 * PRIVATE function
 * draw a line from the point (x1,y1) to (x2,y2)
 * @param x1
 * @param x2
 * @param y1
 * @param y2
 */
function draw_y(x1, x2, y1, y2) {
	console.log("draw_y-> i'm drawing y : from ("+x1+","+x2+") to ("+y1+","+y2+")");

	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.strokeStyle = pen_color;
	ctx.stroke();
//	for(var y = y1; y <= y2; y++)
//	ctx.add(x1 + ((x2 - x1) * (y - y1)) / (y2 - y1), y, pen_color);
}



//function myDraw(x1,y1,x2,y2){
//ctx.beginPath();
//ctx.moveTo(x1,y1);
//ctx.lineTo(x2,y2);
//ctx.stroke();
//}

/*=============================================================*/
/*				   PUBLIC FUNCTIONS	        				 */
/*=============================================================*/


/**
 * PUBLIC function 
 * Efface toutes traces du carré de salade de taille (512, 512). 
 */
function clear_all() {
	ctx.clearRect(0, 0, width, height);
}


/** 
 * PUBLIC function
 * Retour au milieu du carré de salade, au point (256, 256). 
 */
function home(){
	update(width / 2, height / 2);
}

function position(){
	console.log("position : ("+canvas_x+","+""+canvas_y+") angle : "+angle+", panting : "+pen);
}

/** 
 * PUBLIC function
 * La tortue avance de n pas. 
 */
function forward(n) {
	set_position(canvas_x + n * Math.cos(angle), canvas_y + n * Math.sin(angle));
	//set_position(canvas_x  + n * Math.cos(angle) +n, canvas_y);
}

/**
 * PUBLIC function 
 * La tortue recule de n pas. 
 */
function backward(n) {
	forward(-n);
}

/** 
 * PUBLIC function
 * La tortue tourne de n degrés d'angle vers la gauche. 
 */
function leftward(n) {
	console.log("i'm turning left of "+n+" degrees");
	angle -= Math.PI / 180.0 * n;
}

/** 
 * PUBLIC function
 * La tortue tourne de n degrés d'angle vers la droite. 
 */
function rightward(n) {
	console.log("i'm turning right of "+n+" degrees");
	leftward(-n);
}

/** 
 * PUBLIC function
 * Fixe la position absolue de la tortue dans le carré de salade. 
 */
function set_position(x, y) {
	update( x, y);
}

/**
 * PUBLIC function 
 * Fixe le cap de la tortue de maniere absolue, selon l'angle de a degrés. 
 */
function set_heading(a) {
	angle = Math.PI / 180.0 * a;
}

/** 
 * PUBLIC function
 * La tortue ne laisse pas de trace. 
 */
function pen_up() {
	pen = false;
}

/** 
 * PUBLIC function
 * La tortue laisse sa trace (par défaut). 
 */
function pen_down() {
	pen = true;
}


/** 
 * PUBLIC function
 * Change la couleur du fond, n est un entier positif entre 0 et 9.
 * @param n : 0 (noir), 1 (brun), 2 (rouge), 3 (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9 (blanc).
 */
function set_background(color) {
	//TODO to finish to implement
//	ctx.fillStyle   = '#CC5422'; // set canvas background color
//	ctx.fillRect  (0,   0, width, height);  // now fill the canvas 
	//ctx.setBackground(colors[n < 0 || n > 9 ? 0 : n]);
	canvas.style.backgroundColor = color;
}

/** 
 * PUBLIC function
 * Change la couleur du crayon, n est un entier positif entre 0 et 9.
 * @param n : 0 (noir), 1 (brun), 2 (rouge), 3 (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9 (blanc).
 */
function set_color(color) {
	pen_color = color;
}

function showTurtle(x, y){
	if(turtle_shown){
		console.log("showing turle in position : ("+x+","+y+")")
		imageObj.onload = function() {
//ctx.drawImage(imageObj, 69, 50);
			ctx.drawImage(imageObj, x, y);	
		};
		
	}
}

function show_turtle() {
	//TODO to translate in javascript
	turtle_shown = true;
	show_turtle( canvas_x, canvas_y);
}


function hide_turtle() {
	//TODO to translate in javascript
	turtle_shown = false;
	show_turtle(canvas_x, canvas_y);
}

//});