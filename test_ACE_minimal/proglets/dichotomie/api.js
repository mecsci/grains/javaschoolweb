/** Définit les fonctions de la proglet qui permet d'implémenter un algorithme de dichotomie.
 *
 */


/** Renvoie le nombre de page du livre. */
function length() {
	return org.javascool.proglets.dichotomie.Panel.pays.length;
}

/** Ouvre le livre à une page et compare un nom au nom affiché sur cette page.
 * @param name Le nom à comparer.
 * @param index L'index de la page, de 0 à length() exclu.
 * @return -1 si le nom se situe avant celui de la page, +1 si le nom se situe après celui de la page, 0 si il correspond à celui de la page.
 */
function compare(name, index) {
	index = getPane().show(index);
	return noAccent(name).compareTo(noAccent(org.javascool.proglets.dichotomie.Panel.pays[index][0]));
}

